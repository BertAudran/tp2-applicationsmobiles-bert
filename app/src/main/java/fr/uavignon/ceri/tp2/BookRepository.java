package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<Book> selectedBook =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;

    public BookRepository(Application application){

        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
        System.out.println("XXX ICI XXX");
        System.out.println(allBooks.getValue());
    }

    public LiveData<List<Book>> getAllBooks(){
        return allBooks;
    }

    public  MutableLiveData<Book> getSelectedBook(){
        return  selectedBook;
    }

    public void updateBook(Book book){
        databaseWriteExecutor.execute(()->{
           bookDao.updateBook(book);
        });
    }

    public void insertBook(Book book){
        databaseWriteExecutor.execute(()->{
            bookDao.insertBook(book);
        });
    }

    public void getBook(long id){
        Future<Book> fbook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /*

    public void getAllBooks(){
        Future<List<Book>> books = databaseWriteExecutor.submit(() -> {
            return bookDao.getAllBooks();
        });
        try {
            allBooks.setvalue(books.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
*/

}
