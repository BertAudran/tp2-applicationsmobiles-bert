package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel {
    private BookRepository repository;
    private LiveData<List<Book>> books;

    public ListViewModel(@NonNull Application application) {
        super(application);
        System.out.println("XXX CREATEREPO XXX");
        repository= new BookRepository(application);
        books=repository.getAllBooks();
        System.out.println("XXX REPOCREATED XXX");
        System.out.println(books.getValue());
    }

    public LiveData<List<Book>> getAllBooks() {
        return books;
    }
}
