package fr.uavignon.ceri.tp2;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Collection;
import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

@Dao
public interface BookDao {

    @Update
    public void updateBook(Book book);

    @Insert
    public void insertBook(Book book);

    @Query("SELECT * from books where id=:id;")
    public Book getBook(long id);

    @Query("DELETE from books where id=:id;")
    public void deleteBook(long id) ;
    

    @Query("SELECT * FROM books;")
    public LiveData<List<Book>> getAllBooks();


    @Query("DELETE FROM books;")
    public void deleteAllBooks();
}
