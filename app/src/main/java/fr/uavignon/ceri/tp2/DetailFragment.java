package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {
    private DetailViewModel viewModel;
    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int id=(int)args.getBookNum();
        viewModel =new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        System.out.println("ARG="+id);
        if (id!=-1) {
            viewModel.setBookToDisplay(id);
            Book book = viewModel.getBook().getValue();
            textTitle.setText(book.getTitle());
            textAuthors.setText(book.getAuthors());
            textYear.setText(book.getYear());
            textGenres.setText(book.getGenres());
            textPublisher.setText(book.getPublisher());
            observerSetup();
        }
        else{

        }
        //Book book = Book.books[(int)args.getBookNum()];













        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textTitle.getText().toString().equals("") || textAuthors.getText().toString().equals("") || textYear.getText().toString().equals("") || textGenres.getText().toString().equals("") || textPublisher.getText().toString().equals("")){
                    Snackbar.make(v, "Il manque un champ !", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }else {
                    System.out.println("M"+textAuthors.getText().toString());
                    viewModel.insertOrUpdateBook( textTitle.getText().toString(),textAuthors.getText().toString(),textYear.getText().toString(),textGenres.getText().toString(),textPublisher.getText().toString());
                    NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                            .navigate(R.id.action_SecondFragment_to_FirstFragment);
                }


            }
        });
    }
    private void observerSetup() {
        viewModel.getBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {

                    @Override
                    public void onChanged(Book book) {
                        textTitle.setText(book.getTitle());
                        textAuthors.setText(book.getAuthors());
                        textYear.setText(book.getYear());
                        textGenres.setText(book.getGenres());
                        textPublisher.setText(book.getPublisher());
                    }
                });
    }
}