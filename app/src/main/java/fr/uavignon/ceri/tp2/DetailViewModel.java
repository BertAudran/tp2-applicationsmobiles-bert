package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.text.Editable;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> book;

    public DetailViewModel(@NonNull Application application) {
        super(application);

        repository= new BookRepository(application);

    }

    public void setBookToDisplay(int id){
        repository.getBook(id);
        book=repository.getSelectedBook();
        System.out.println(book.getValue().getTitle());
    }

    public void insertOrUpdateBook(String title, String authors, String year, String genre, String publisher){

        if (book!=null){
            Book updatedBook = book.getValue();
            updatedBook.setTitle(title);
            updatedBook.setAuthors(authors);
            updatedBook.setYear(year);
            updatedBook.setGenres(genre);
            updatedBook.setPublisher(publisher);
            repository.updateBook(updatedBook);
        }
        else {
                Book newBook= new Book(title,authors,year,genre,publisher);
                repository.insertBook(newBook);

        }
    }

    public MutableLiveData<Book> getBook() {
        return  book;
    }


    public void setBook(MutableLiveData<Book> book) {
        this.book = book;
    }

    public BookRepository getRepository(){
        return repository;
    }
}
